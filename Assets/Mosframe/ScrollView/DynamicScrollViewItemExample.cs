﻿/**
 * DynamicScrollViewItemExample.cs
 * 
 * @author mosframe / https://github.com/mosframe
 * 
 */

namespace Mosframe
{

    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class DynamicScrollViewItemExample : UIBehaviour, IDynamicScrollViewItem
    {
        public Text title;
        //public Image background;
        [SerializeField] DataLoader DataLoader;
        [SerializeField] private RoomViewManager RoomViewManager;
        [SerializeField] private SuperGameViewManager SuperGameViewManager;

        public void onUpdateItem(int index)
        {
            //Debug.LogError(index + " --- index");
            this.title.text = string.Format("Name{0:d3}", (index + 1));

            if (DataLoader.PlayersList.Count > 0 && index < DataLoader.PlayersList.Count)
            {
                if (DataLoader.PlayersList[index].Room != null)
                {
                   
                    //Debug.LogError(DataLoader.PlayersList[index].Room + " --- DataLoader.PlayersList[index].Room ");
                    RoomViewManager.gameObject.SetActive(true);
                    SuperGameViewManager.gameObject.SetActive(false);
                    RoomViewManager.Init(DataLoader.PlayersList[index].Room, index);
                }
                else if (DataLoader.PlayersList[index].SuperGame != null)
                {
                    RoomViewManager.gameObject.SetActive(false);
                    SuperGameViewManager.gameObject.SetActive(true);
                    SuperGameViewManager.Init(DataLoader.PlayersList[index].SuperGame);
                }
            }


        }
    }
}