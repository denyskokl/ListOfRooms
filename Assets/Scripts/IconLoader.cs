﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IconLoader : MonoBehaviour
{
    private Dictionary<string, Sprite> Icons = new Dictionary<string, Sprite>();

    public Sprite GetImage(string name)
    {
        if (Icons.ContainsKey(name))
        {
            return Icons[name];
        }
        return null;
    }

    public void DownloadImage(string name, string url)
    {
        StartCoroutine(Downloading(name, url));
    }

    private IEnumerator Downloading(string name, string url)
    {
        Texture2D texture = new Texture2D(4, 4, TextureFormat.DXT1, false);
        WWW www = new WWW(url);
        yield return www;
        www.LoadImageIntoTexture(texture);
        Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
        Icons.Add(name, sprite);
    }
}
