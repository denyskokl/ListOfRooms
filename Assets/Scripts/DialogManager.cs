﻿using System;
using UnityEngine;
using UnityEngine.UI;
public class DialogManager : MonoBehaviour
{
    [SerializeField] private Text MessageText;
    [SerializeField] private Button CloseButton;

    public void SetMessage(string message)
    {
        MessageText.text = message;
    }

    public void CloseApp()
    {
        CloseButton.onClick.AddListener(QuitApp);
    }

    private void QuitApp()
    {
        Application.Quit();
    }
}
