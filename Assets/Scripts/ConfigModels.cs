﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;

public class ConfigData
{
    [XmlElement(ElementName = "url")]
    public string Url;
    [XmlElement(ElementName = "games")]
    public int[] Games;
}

[XmlRoot(ElementName = "data")]
public class MainData
{
    [XmlElement(ElementName = "settings")]
    public Settings Settings { get; set; }
    [XmlElement(ElementName = "roomlist")]
    public RoomList RoomList { get; set; }
}

[XmlRoot(ElementName = "settings")]
public class Settings
{
    [XmlElement(ElementName = "param")]
    public List<Param> Param { get; set; }
}

[XmlRoot(ElementName = "roomlist")]
public class RoomList
{
    [XmlElement(ElementName = "game")]
    public List<Game> Game { get; set; }
}

[XmlRoot(ElementName = "game")]
public class Game
{
    [XmlElement(ElementName = "supergame")]
    public List<SuperGame> SuperGame { get; set; }
    [XmlElement(ElementName = "room")]
    public List<Room> Room { get; set; }
    [XmlAttribute(AttributeName = "name")]
    public string Name { get; set; }
    [XmlAttribute(AttributeName = "players")]
    public int Players { get; set; }
    [XmlAttribute(AttributeName = "gameId")]
    public string GameId { get; set; }
}

[XmlRoot(ElementName = "param")]
public class Param
{
    [XmlAttribute(AttributeName = "name")]
    public string Name { get; set; }
    [XmlText]
    public string Text { get; set; }
}

[XmlRoot(ElementName = "supergame")]
public class SuperGame
{
    [XmlAttribute(AttributeName = "price")]
    public int Price { get; set; }
    [XmlAttribute(AttributeName = "max")]
    public int Max { get; set; }
    [XmlAttribute(AttributeName = "current")]
    public int Current { get; set; }
}

[XmlRoot(ElementName = "room")]
public class Room
{
    [XmlAttribute(AttributeName = "players")]
    public int Players { get; set; }
    [XmlAttribute(AttributeName = "maxPlayers")]
    public int MaxPlayers { get; set; }
    [XmlAttribute(AttributeName = "price")]
    public int Price { get; set; }
    [XmlText]
    public string Text { get; set; }
}

public class ListPlayers
{
    public SuperGame SuperGame = null;
    public Room Room = null;

    public ListPlayers(SuperGame superGame)
    {
        this.SuperGame = superGame;
        Room = null;
    }
    public ListPlayers(Room room)
    {
        this.Room = room;
        SuperGame = null;
    }
}