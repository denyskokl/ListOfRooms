﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomViewManager : MonoBehaviour {

    [SerializeField] private IconLoader IconLoader;
    [SerializeField] private Image PlayerIcon;
    [SerializeField] private Text PlayerText;
    [SerializeField] private Text RoomName;
    [SerializeField] private GameObject FullParty;
    [SerializeField] private ViewManager ViewManager;

    private string NameOfRoom = string.Empty;


    public void Init(Room room, int index)
    {
        NameOfRoom = room.Text;
        PlayerIcon.sprite = IconLoader.GetImage("PLAYERS_ICON");
        PlayerText.text = room.Players.ToString();
        RoomName.text = room.Text;
        FullParty.SetActive(room.Players >= room.MaxPlayers);
    }

    public void JoinInToRoom()
    {
        ViewManager.ShowDialog(NameOfRoom);
    }
    
}
