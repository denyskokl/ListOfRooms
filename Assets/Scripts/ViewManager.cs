﻿using System;
using UnityEngine;
using UnityEngine.Events;
using Utils.TypedUnityEvents;
using UnityEngine.UI;

public class ViewManager : MonoBehaviour
{
    [SerializeField] private Dropdown Dropdown;
    [SerializeField] private DataLoader DataLoader;
    [SerializeField] private Events ViewEvents;

    public void UpdateInfo(MainData data)
    {
        Dropdown.ClearOptions();
        Dropdown.OptionDataList newOptions = new Dropdown.OptionDataList();
        foreach (var item in data.RoomList.Game)
        {
            Dropdown.OptionData option = new Dropdown.OptionData(item.Name);
            newOptions.options.Add(option);
        }
        Dropdown.AddOptions(newOptions.options);

        Invoke("ShowData", 1);
    }

    public void ShowData()
    {
        ChangeRoom(0);

    }

    public void ChangeRoom(int numberOfList)
    {
        DataLoader.ChooseRoom(numberOfList);
    }

    public void ShowDialog(string message)
    {
        ViewEvents.SetText.Invoke(message);
    }

    [Serializable]
    private class Events
    {
        public StringUnityEvent SetText;
    }

}


