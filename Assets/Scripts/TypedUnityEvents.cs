﻿using System;
using UnityEngine.Events;

namespace Utils.TypedUnityEvents
{
    [Serializable] public class StringUnityEvent : UnityEvent<string> { }
}