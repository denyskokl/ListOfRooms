﻿using UnityEngine;
using UnityEngine.UI;
public class SuperGameViewManager : MonoBehaviour {

    [SerializeField] private IconLoader IconLoader;
    [SerializeField] private Image CoinImage;
    [SerializeField] private Text Value;
    [SerializeField] private Slider Slider;
    [SerializeField] private Text PBValue;

    public void Init(SuperGame superGame)
    {
        CoinImage.sprite = IconLoader.GetImage("COIN_ICON");
        Value.text = superGame.Price.ToString();
        Slider.value = (float)superGame.Current / superGame.Max;
        PBValue.text = string.Format("{0}/{1}", superGame.Current, superGame.Max);
    }
}
