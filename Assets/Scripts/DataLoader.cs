﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.IO;
using System;
using System.Linq;
using Mosframe;
using Newtonsoft.Json;

public class DataLoader : MonoBehaviour
{
    [SerializeField] private ViewManager ViewManager;
    [SerializeField] private DialogManager DialogManager;
    [SerializeField] private IconLoader IconLoader;
    public ConfigData Config;
    public MainData Data;
    public DynamicScrollView DynamicScrollView;
    public List<ListPlayers> PlayersList = new List<ListPlayers>();
    private string ConfigArgs;

    void Start()
    {
        LoadMainInfo(() =>
        {
            foreach (var item in Data.Settings.Param)
            {
                IconLoader.DownloadImage(item.Name, item.Text);
            }

            ViewManager.UpdateInfo(Data);
        });
    }

    public void LoadMainInfo(Action OnSucces)
    {
        SettingsForDownloading();
        StartCoroutine(ProcessingData(Config.Url, OnSucces));
    }

    public void ChooseRoom(int numberOfList)
    {
        InitList(numberOfList);
    }

    private void InitList(int numberOfList)
    {
        PlayersList.Clear();
        Game game = Data.RoomList.Game[numberOfList];
        SortList(game);
    }

    private void SortList(Game game)
    {
        List<Room> roomlist = new List<Room>();

        roomlist = game.Room.OrderBy(o => o.Players).ToList();

        foreach (var item in game.SuperGame)
        {
            PlayersList.Add(new ListPlayers(item));
            foreach (var room in roomlist)
            {
                if (room.Price == item.Price)
                    PlayersList.Add(new ListPlayers(room));
            }
        }
        DynamicScrollView.totalItemCount = PlayersList.Count;
    }

    public void SettingsForDownloading()
    {
#if !UNITY_EDITOR
        LoadConfig();
#elif UNITY_EDITOR
        Config = new ConfigData();
        Config.Url = "http://test.gamepoint.com.ua/roomlist/roomlist.xml";
#endif 
    }

    public void LoadConfig()
    {
        if (!string.IsNullOrEmpty(GetArguments("-config")))
        {
            ConfigArgs = string.Format("{0}/{1}", Application.dataPath, GetArguments("-config"));
            if (File.Exists(ConfigArgs))
            {
                StreamReader reader = new StreamReader(ConfigArgs);
                Config = JsonConvert.DeserializeObject<ConfigData>(reader.ReadToEnd());
                reader.Close();
            }
            else
            {
                FailLoadData();
            }
        }
        else
        {
            FailLoadData();
        }
    }

    private void FailLoadData()
    {
        DialogManager.CloseApp();
        ViewManager.ShowDialog("Put the correct file, and reload the app");
    }

    private string GetArguments(string name)
    {
        var arguments = Environment.GetCommandLineArgs();
        for (int i = 0; i < arguments.Length; i++)
        {
            if (arguments[i] == name && arguments.Length > i + 1)
            {
                return arguments[i + 1];
            }
        }
        return string.Empty;
    }

    IEnumerator ProcessingData(string url, Action OnSucces)
    {
        WWW www = new WWW(url);
        yield return www;

        if (!string.IsNullOrEmpty(www.text))
        {
            var serializer = new XmlSerializer(typeof(MainData));
            Data = serializer.Deserialize(new StringReader(www.text)) as MainData;

            if (OnSucces != null)
                OnSucces();
        }
        yield return null;
    }

}
